import 'package:crypto_stats/crypto_stats/presentation/widgets/app_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils/material_wrapper.dart';

void main() {
  testWidgets('should find the message and user namein the widget',
      (WidgetTester tester) async {
    const message = 'Welcome';
    const user = 'Kleyton';
    await tester.pumpWidget(const MaterialWrapper(
        widget: AppHeader(message: message, username: user)));
    expect(find.text(message), findsOneWidget);
    expect(find.text(user), findsOneWidget);
    expect(find.byType(CircleAvatar), findsOneWidget);
  });
}
