import 'package:crypto_stats/crypto_stats/presentation/colors/crypto_colors.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/balance_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils/material_wrapper.dart';

void main() {
  const keyValue = 'balanceCardKey';
  Widget balanceCard;
  testWidgets('should find the balance informations',
      (WidgetTester tester) async {
    balanceCard = const BalanceCard(
      keyValue: keyValue,
      balance: 450000,
      profit: 10000,
      variation: 0.1,
    );
    await tester.pumpWidget(MaterialWrapper(widget: balanceCard));
    expect(find.text(r'$450,000'), findsOneWidget);
    expect(find.text(r'$10,000'), findsOneWidget);
    expect(find.text('10%'), findsOneWidget);
  });

  testWidgets('should assert the background color of the card',
      (WidgetTester tester) async {
    balanceCard = const BalanceCard(
      keyValue: keyValue,
      balance: 450000,
      profit: 10000,
      variation: 0.1,
    );
    await tester.pumpWidget(MaterialWrapper(widget: balanceCard));
    expect(
        (((tester.firstWidget(find.byType(Container)) as Container).decoration)
                as BoxDecoration)
            .color,
        CryptoColors.balanceBackground);
  });

  testWidgets('should find a green color for the variation icon',
      (WidgetTester tester) async {
    balanceCard = const BalanceCard(
      keyValue: keyValue,
      balance: 450000,
      profit: 10000,
      variation: 0.1,
    );
    await tester.pumpWidget(MaterialWrapper(widget: balanceCard));
    expect(((tester.firstWidget(find.byType(Icon))) as Icon).color,
        CryptoColors.positiveColor);
  });

  testWidgets('should find a red color for the negative variation icon',
      (WidgetTester tester) async {
    balanceCard = const BalanceCard(
      keyValue: keyValue,
      balance: 450000,
      profit: 10000,
      variation: -0.05,
    );
    await tester.pumpWidget(MaterialWrapper(widget: balanceCard));
    expect(((tester.firstWidget(find.byType(Icon))) as Icon).color,
        CryptoColors.negativeColor);
  });
}
