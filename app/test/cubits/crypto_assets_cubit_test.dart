import 'package:bloc_test/bloc_test.dart';
import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_assets.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_assets_cubit.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_assets_state.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../data/mocks.dart';
import 'crypto_assets_cubit_test.mocks.dart';

@GenerateMocks([GetCryptoAssetsUseCase])
void main() {
  late CryptoAssetsCubit _cryptoAssetsCubit;
  late GetCryptoAssetsUseCase _getCryptoAssetsUseCase;
  final mockedResponse = CryptoMocks.assetsResponse
      .map((e) => CryptoAssetModel.fromJson(e))
      .toList();

  setUp(() {
    _getCryptoAssetsUseCase = MockGetCryptoAssetsUseCase();
    _cryptoAssetsCubit = CryptoAssetsCubit(_getCryptoAssetsUseCase);
  });

  blocTest<CryptoAssetsCubit, CryptoAssetsState>(
    'gets a success state with the crypto assets',
    build: () {
      when(_getCryptoAssetsUseCase())
          .thenAnswer((_) => Future.value(mockedResponse));
      return _cryptoAssetsCubit;
    },
    act: (cubit) async => cubit.getCryptoAssets(),
    expect: () => [
      const CryptoAssetsState.loading(),
      CryptoAssetsState.success(mockedResponse)
    ],
  );

  blocTest<CryptoAssetsCubit, CryptoAssetsState>(
    'gets a failure state if any exception is throwed',
    build: () {
      when(_getCryptoAssetsUseCase()).thenThrow(Exception());
      return _cryptoAssetsCubit;
    },
    act: (cubit) async => cubit.getCryptoAssets(),
    expect: () =>
        [const CryptoAssetsState.loading(), const CryptoAssetsState.failure()],
  );
}
