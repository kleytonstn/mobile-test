import 'package:crypto_stats/crypto_stats/data/datasources/crypto_remote_datasource.dart';
import 'package:crypto_stats/crypto_stats/data/repository/crypto_repository_impl.dart';
import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:crypto_stats/crypto_stats/domain/repositories/crypto_repository.dart';
import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_assets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../data/mocks.dart';
import 'get_crypto_assets_test.mocks.dart';

@GenerateMocks([CryptoRemoteDataSource])
void main() {
  late CryptoRemoteDataSource _cryptoRemoteDataSourceMock;
  late CryptoRepository _cryptoRepository;
  late GetCryptoAssetsUseCase _getCryptoAssetsUseCase;

  setUp(() {
    _cryptoRemoteDataSourceMock = MockCryptoRemoteDataSource();
    _cryptoRepository = CryptoRepositoryImpl(_cryptoRemoteDataSourceMock);
    _getCryptoAssetsUseCase = GetCryptoAssetsUseCase(_cryptoRepository);

    when(
      _cryptoRemoteDataSourceMock.getCryptoAssets(),
    ).thenAnswer((_) => Future.value(CryptoMocks.assetsResponse));
  });

  test('should get the crypto assets', () async {
    final response = await _getCryptoAssetsUseCase();
    expect(response, isNotNull);
    expect(response, isNotEmpty);
    expect(response, isA<List<CryptoAssetModel>>());
    expect(response.first, isNotNull);
    expect(response.first, isA<CryptoAssetModel>());
  });
}
