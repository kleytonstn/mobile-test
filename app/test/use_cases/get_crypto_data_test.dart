import 'package:crypto_stats/crypto_stats/data/constants/endpoints.dart';
import 'package:crypto_stats/crypto_stats/data/constants/ws_messages.dart';
import 'package:crypto_stats/crypto_stats/data/datasources/crypto_remote_datasource.dart';
import 'package:crypto_stats/crypto_stats/data/repository/crypto_repository_impl.dart';
import 'package:crypto_stats/crypto_stats/domain/repositories/crypto_repository.dart';
import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_stream.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_crypto_data_test.mocks.dart';

@GenerateMocks([CryptoRemoteDataSource])
void main() {
  late CryptoRemoteDataSource _cryptoRemoteDataSourceMock;
  late CryptoRepository _cryptoRepository;
  late GetCryptoStreamUseCase _getCryptoStreamUseCase;

  setUp(() {
    _cryptoRemoteDataSourceMock = MockCryptoRemoteDataSource();
    _cryptoRepository = CryptoRepositoryImpl(_cryptoRemoteDataSourceMock);
    _getCryptoStreamUseCase = GetCryptoStreamUseCase(_cryptoRepository);

    when(
      _cryptoRemoteDataSourceMock.getCryptoStream(
          Endpoints.cryptoWebSocket, WsMessages.cryptoMessage),
    ).thenAnswer((_) => Future.value(
        Stream<int>.periodic(const Duration(seconds: 1), (x) => x).take(10)));
  });

  test('should get the stream of data', () async {
    final stream = await _getCryptoStreamUseCase();
    expect(stream, isNotNull);
    expect(stream, isA<Stream<int>>());
    stream.listen((dynamic event) {
      expect(event, isNotNull);
      expect(event, isA<int>());
      expect(event, 1);
    });
  });
}
