import 'package:crypto_stats/crypto_stats/crypto_stats.dart';
import 'package:crypto_stats/crypto_stats/data/constants/strings_constants.dart';
import 'package:crypto_stats/crypto_stats/data/datasources/crypto_remote_datasource_impl.dart';
import 'package:crypto_stats/crypto_stats/data/repository/crypto_repository_impl.dart';
import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_assets.dart';
import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_stream.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_assets_cubit.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_cubit.dart';
import 'package:crypto_stats/crypto_stats/presentation/views/empty_api_key_view.dart';
import 'package:crypto_stats/generated/fonts.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

/// {@template app}
/// The widget that handles the dependency injection of your application.
/// {@endtemplate}
class App extends StatelessWidget {
  /// {@macro app}
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const _apiKey = String.fromEnvironment(StringConstants.envKey);
    if (_apiKey.isEmpty) return const EmptyApiKeyView();
    final _cryptoRepository =
        CryptoRepositoryImpl(CryptoRemoteDataSourceImpl(_apiKey));
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) =>
                CryptoCubit(GetCryptoStreamUseCase(_cryptoRepository))),
        BlocProvider(
            create: (context) =>
                CryptoAssetsCubit(GetCryptoAssetsUseCase(_cryptoRepository))),
      ],
      child: const AppView(),
    );
  }
}

/// {@template app_view}
/// The widget that configures your application.
/// {@endtemplate}
class AppView extends StatelessWidget {
  /// {@macro app_view}
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      restorationScopeId: 'app',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''), // English, no country code.
      ],
      theme: ThemeData(
          fontFamily: FontFamily.poppins,
          textTheme: const TextTheme(
            bodyText1: TextStyle(fontSize: 16),
            bodyText2: TextStyle(fontSize: 14, color: Colors.white),
          )),
      darkTheme: ThemeData.dark(),
      themeMode: ThemeMode.light,
      home: const CryptoStatsPage(),
    );
  }
}
