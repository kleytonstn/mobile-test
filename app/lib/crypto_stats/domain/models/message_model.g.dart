// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MessageModel _$$_MessageModelFromJson(Map<String, dynamic> json) =>
    _$_MessageModel(
      type: json['type'] as String?,
      apikey: json['apikey'] as String?,
      heartbeat: json['heartbeat'] as bool?,
      subscribe_data_type: (json['subscribe_data_type'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      subscribe_filter_symbol_id:
          (json['subscribe_filter_symbol_id'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList(),
    );

Map<String, dynamic> _$$_MessageModelToJson(_$_MessageModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'apikey': instance.apikey,
      'heartbeat': instance.heartbeat,
      'subscribe_data_type': instance.subscribe_data_type,
      'subscribe_filter_symbol_id': instance.subscribe_filter_symbol_id,
    };
