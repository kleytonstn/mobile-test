// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'crypto_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CryptoModel _$CryptoModelFromJson(Map<String, dynamic> json) {
  return _CryptoModel.fromJson(json);
}

/// @nodoc
class _$CryptoModelTearOff {
  const _$CryptoModelTearOff();

  _CryptoModel call(
      {@JsonKey(name: 'time_exchange') String? timeExchange,
      @JsonKey(name: 'time_coinapi') String? timeCoinapi,
      String? uuid,
      double? price,
      double? size,
      @JsonKey(name: 'taker_side') String? takerSide,
      @JsonKey(name: 'symbol_id') String? symbolId,
      int? sequence,
      String? type}) {
    return _CryptoModel(
      timeExchange: timeExchange,
      timeCoinapi: timeCoinapi,
      uuid: uuid,
      price: price,
      size: size,
      takerSide: takerSide,
      symbolId: symbolId,
      sequence: sequence,
      type: type,
    );
  }

  CryptoModel fromJson(Map<String, Object> json) {
    return CryptoModel.fromJson(json);
  }
}

/// @nodoc
const $CryptoModel = _$CryptoModelTearOff();

/// @nodoc
mixin _$CryptoModel {
  @JsonKey(name: 'time_exchange')
  String? get timeExchange => throw _privateConstructorUsedError;
  @JsonKey(name: 'time_coinapi')
  String? get timeCoinapi => throw _privateConstructorUsedError;
  String? get uuid => throw _privateConstructorUsedError;
  double? get price => throw _privateConstructorUsedError;
  double? get size => throw _privateConstructorUsedError;
  @JsonKey(name: 'taker_side')
  String? get takerSide => throw _privateConstructorUsedError;
  @JsonKey(name: 'symbol_id')
  String? get symbolId => throw _privateConstructorUsedError;
  int? get sequence => throw _privateConstructorUsedError;
  String? get type => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CryptoModelCopyWith<CryptoModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CryptoModelCopyWith<$Res> {
  factory $CryptoModelCopyWith(
          CryptoModel value, $Res Function(CryptoModel) then) =
      _$CryptoModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'time_exchange') String? timeExchange,
      @JsonKey(name: 'time_coinapi') String? timeCoinapi,
      String? uuid,
      double? price,
      double? size,
      @JsonKey(name: 'taker_side') String? takerSide,
      @JsonKey(name: 'symbol_id') String? symbolId,
      int? sequence,
      String? type});
}

/// @nodoc
class _$CryptoModelCopyWithImpl<$Res> implements $CryptoModelCopyWith<$Res> {
  _$CryptoModelCopyWithImpl(this._value, this._then);

  final CryptoModel _value;
  // ignore: unused_field
  final $Res Function(CryptoModel) _then;

  @override
  $Res call({
    Object? timeExchange = freezed,
    Object? timeCoinapi = freezed,
    Object? uuid = freezed,
    Object? price = freezed,
    Object? size = freezed,
    Object? takerSide = freezed,
    Object? symbolId = freezed,
    Object? sequence = freezed,
    Object? type = freezed,
  }) {
    return _then(_value.copyWith(
      timeExchange: timeExchange == freezed
          ? _value.timeExchange
          : timeExchange // ignore: cast_nullable_to_non_nullable
              as String?,
      timeCoinapi: timeCoinapi == freezed
          ? _value.timeCoinapi
          : timeCoinapi // ignore: cast_nullable_to_non_nullable
              as String?,
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      size: size == freezed
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as double?,
      takerSide: takerSide == freezed
          ? _value.takerSide
          : takerSide // ignore: cast_nullable_to_non_nullable
              as String?,
      symbolId: symbolId == freezed
          ? _value.symbolId
          : symbolId // ignore: cast_nullable_to_non_nullable
              as String?,
      sequence: sequence == freezed
          ? _value.sequence
          : sequence // ignore: cast_nullable_to_non_nullable
              as int?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$CryptoModelCopyWith<$Res>
    implements $CryptoModelCopyWith<$Res> {
  factory _$CryptoModelCopyWith(
          _CryptoModel value, $Res Function(_CryptoModel) then) =
      __$CryptoModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'time_exchange') String? timeExchange,
      @JsonKey(name: 'time_coinapi') String? timeCoinapi,
      String? uuid,
      double? price,
      double? size,
      @JsonKey(name: 'taker_side') String? takerSide,
      @JsonKey(name: 'symbol_id') String? symbolId,
      int? sequence,
      String? type});
}

/// @nodoc
class __$CryptoModelCopyWithImpl<$Res> extends _$CryptoModelCopyWithImpl<$Res>
    implements _$CryptoModelCopyWith<$Res> {
  __$CryptoModelCopyWithImpl(
      _CryptoModel _value, $Res Function(_CryptoModel) _then)
      : super(_value, (v) => _then(v as _CryptoModel));

  @override
  _CryptoModel get _value => super._value as _CryptoModel;

  @override
  $Res call({
    Object? timeExchange = freezed,
    Object? timeCoinapi = freezed,
    Object? uuid = freezed,
    Object? price = freezed,
    Object? size = freezed,
    Object? takerSide = freezed,
    Object? symbolId = freezed,
    Object? sequence = freezed,
    Object? type = freezed,
  }) {
    return _then(_CryptoModel(
      timeExchange: timeExchange == freezed
          ? _value.timeExchange
          : timeExchange // ignore: cast_nullable_to_non_nullable
              as String?,
      timeCoinapi: timeCoinapi == freezed
          ? _value.timeCoinapi
          : timeCoinapi // ignore: cast_nullable_to_non_nullable
              as String?,
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as double?,
      size: size == freezed
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as double?,
      takerSide: takerSide == freezed
          ? _value.takerSide
          : takerSide // ignore: cast_nullable_to_non_nullable
              as String?,
      symbolId: symbolId == freezed
          ? _value.symbolId
          : symbolId // ignore: cast_nullable_to_non_nullable
              as String?,
      sequence: sequence == freezed
          ? _value.sequence
          : sequence // ignore: cast_nullable_to_non_nullable
              as int?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CryptoModel implements _CryptoModel {
  const _$_CryptoModel(
      {@JsonKey(name: 'time_exchange') this.timeExchange,
      @JsonKey(name: 'time_coinapi') this.timeCoinapi,
      this.uuid,
      this.price,
      this.size,
      @JsonKey(name: 'taker_side') this.takerSide,
      @JsonKey(name: 'symbol_id') this.symbolId,
      this.sequence,
      this.type});

  factory _$_CryptoModel.fromJson(Map<String, dynamic> json) =>
      _$$_CryptoModelFromJson(json);

  @override
  @JsonKey(name: 'time_exchange')
  final String? timeExchange;
  @override
  @JsonKey(name: 'time_coinapi')
  final String? timeCoinapi;
  @override
  final String? uuid;
  @override
  final double? price;
  @override
  final double? size;
  @override
  @JsonKey(name: 'taker_side')
  final String? takerSide;
  @override
  @JsonKey(name: 'symbol_id')
  final String? symbolId;
  @override
  final int? sequence;
  @override
  final String? type;

  @override
  String toString() {
    return 'CryptoModel(timeExchange: $timeExchange, timeCoinapi: $timeCoinapi, uuid: $uuid, price: $price, size: $size, takerSide: $takerSide, symbolId: $symbolId, sequence: $sequence, type: $type)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CryptoModel &&
            (identical(other.timeExchange, timeExchange) ||
                const DeepCollectionEquality()
                    .equals(other.timeExchange, timeExchange)) &&
            (identical(other.timeCoinapi, timeCoinapi) ||
                const DeepCollectionEquality()
                    .equals(other.timeCoinapi, timeCoinapi)) &&
            (identical(other.uuid, uuid) ||
                const DeepCollectionEquality().equals(other.uuid, uuid)) &&
            (identical(other.price, price) ||
                const DeepCollectionEquality().equals(other.price, price)) &&
            (identical(other.size, size) ||
                const DeepCollectionEquality().equals(other.size, size)) &&
            (identical(other.takerSide, takerSide) ||
                const DeepCollectionEquality()
                    .equals(other.takerSide, takerSide)) &&
            (identical(other.symbolId, symbolId) ||
                const DeepCollectionEquality()
                    .equals(other.symbolId, symbolId)) &&
            (identical(other.sequence, sequence) ||
                const DeepCollectionEquality()
                    .equals(other.sequence, sequence)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(timeExchange) ^
      const DeepCollectionEquality().hash(timeCoinapi) ^
      const DeepCollectionEquality().hash(uuid) ^
      const DeepCollectionEquality().hash(price) ^
      const DeepCollectionEquality().hash(size) ^
      const DeepCollectionEquality().hash(takerSide) ^
      const DeepCollectionEquality().hash(symbolId) ^
      const DeepCollectionEquality().hash(sequence) ^
      const DeepCollectionEquality().hash(type);

  @JsonKey(ignore: true)
  @override
  _$CryptoModelCopyWith<_CryptoModel> get copyWith =>
      __$CryptoModelCopyWithImpl<_CryptoModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CryptoModelToJson(this);
  }
}

abstract class _CryptoModel implements CryptoModel {
  const factory _CryptoModel(
      {@JsonKey(name: 'time_exchange') String? timeExchange,
      @JsonKey(name: 'time_coinapi') String? timeCoinapi,
      String? uuid,
      double? price,
      double? size,
      @JsonKey(name: 'taker_side') String? takerSide,
      @JsonKey(name: 'symbol_id') String? symbolId,
      int? sequence,
      String? type}) = _$_CryptoModel;

  factory _CryptoModel.fromJson(Map<String, dynamic> json) =
      _$_CryptoModel.fromJson;

  @override
  @JsonKey(name: 'time_exchange')
  String? get timeExchange => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'time_coinapi')
  String? get timeCoinapi => throw _privateConstructorUsedError;
  @override
  String? get uuid => throw _privateConstructorUsedError;
  @override
  double? get price => throw _privateConstructorUsedError;
  @override
  double? get size => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'taker_side')
  String? get takerSide => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'symbol_id')
  String? get symbolId => throw _privateConstructorUsedError;
  @override
  int? get sequence => throw _privateConstructorUsedError;
  @override
  String? get type => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$CryptoModelCopyWith<_CryptoModel> get copyWith =>
      throw _privateConstructorUsedError;
}
