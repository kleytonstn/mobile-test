// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crypto_asset_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CryptoAssetModel _$$_CryptoAssetModelFromJson(Map<String, dynamic> json) =>
    _$_CryptoAssetModel(
      assetId: json['asset_id'] as String?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$$_CryptoAssetModelToJson(_$_CryptoAssetModel instance) =>
    <String, dynamic>{
      'asset_id': instance.assetId,
      'url': instance.url,
    };
