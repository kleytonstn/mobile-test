// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'crypto_asset_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CryptoAssetModel _$CryptoAssetModelFromJson(Map<String, dynamic> json) {
  return _CryptoAssetModel.fromJson(json);
}

/// @nodoc
class _$CryptoAssetModelTearOff {
  const _$CryptoAssetModelTearOff();

  _CryptoAssetModel call(
      {@JsonKey(name: 'asset_id') String? assetId, String? url}) {
    return _CryptoAssetModel(
      assetId: assetId,
      url: url,
    );
  }

  CryptoAssetModel fromJson(Map<String, Object> json) {
    return CryptoAssetModel.fromJson(json);
  }
}

/// @nodoc
const $CryptoAssetModel = _$CryptoAssetModelTearOff();

/// @nodoc
mixin _$CryptoAssetModel {
  @JsonKey(name: 'asset_id')
  String? get assetId => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CryptoAssetModelCopyWith<CryptoAssetModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CryptoAssetModelCopyWith<$Res> {
  factory $CryptoAssetModelCopyWith(
          CryptoAssetModel value, $Res Function(CryptoAssetModel) then) =
      _$CryptoAssetModelCopyWithImpl<$Res>;
  $Res call({@JsonKey(name: 'asset_id') String? assetId, String? url});
}

/// @nodoc
class _$CryptoAssetModelCopyWithImpl<$Res>
    implements $CryptoAssetModelCopyWith<$Res> {
  _$CryptoAssetModelCopyWithImpl(this._value, this._then);

  final CryptoAssetModel _value;
  // ignore: unused_field
  final $Res Function(CryptoAssetModel) _then;

  @override
  $Res call({
    Object? assetId = freezed,
    Object? url = freezed,
  }) {
    return _then(_value.copyWith(
      assetId: assetId == freezed
          ? _value.assetId
          : assetId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$CryptoAssetModelCopyWith<$Res>
    implements $CryptoAssetModelCopyWith<$Res> {
  factory _$CryptoAssetModelCopyWith(
          _CryptoAssetModel value, $Res Function(_CryptoAssetModel) then) =
      __$CryptoAssetModelCopyWithImpl<$Res>;
  @override
  $Res call({@JsonKey(name: 'asset_id') String? assetId, String? url});
}

/// @nodoc
class __$CryptoAssetModelCopyWithImpl<$Res>
    extends _$CryptoAssetModelCopyWithImpl<$Res>
    implements _$CryptoAssetModelCopyWith<$Res> {
  __$CryptoAssetModelCopyWithImpl(
      _CryptoAssetModel _value, $Res Function(_CryptoAssetModel) _then)
      : super(_value, (v) => _then(v as _CryptoAssetModel));

  @override
  _CryptoAssetModel get _value => super._value as _CryptoAssetModel;

  @override
  $Res call({
    Object? assetId = freezed,
    Object? url = freezed,
  }) {
    return _then(_CryptoAssetModel(
      assetId: assetId == freezed
          ? _value.assetId
          : assetId // ignore: cast_nullable_to_non_nullable
              as String?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CryptoAssetModel implements _CryptoAssetModel {
  const _$_CryptoAssetModel(
      {@JsonKey(name: 'asset_id') this.assetId, this.url});

  factory _$_CryptoAssetModel.fromJson(Map<String, dynamic> json) =>
      _$$_CryptoAssetModelFromJson(json);

  @override
  @JsonKey(name: 'asset_id')
  final String? assetId;
  @override
  final String? url;

  @override
  String toString() {
    return 'CryptoAssetModel(assetId: $assetId, url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CryptoAssetModel &&
            (identical(other.assetId, assetId) ||
                const DeepCollectionEquality()
                    .equals(other.assetId, assetId)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(assetId) ^
      const DeepCollectionEquality().hash(url);

  @JsonKey(ignore: true)
  @override
  _$CryptoAssetModelCopyWith<_CryptoAssetModel> get copyWith =>
      __$CryptoAssetModelCopyWithImpl<_CryptoAssetModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CryptoAssetModelToJson(this);
  }
}

abstract class _CryptoAssetModel implements CryptoAssetModel {
  const factory _CryptoAssetModel(
      {@JsonKey(name: 'asset_id') String? assetId,
      String? url}) = _$_CryptoAssetModel;

  factory _CryptoAssetModel.fromJson(Map<String, dynamic> json) =
      _$_CryptoAssetModel.fromJson;

  @override
  @JsonKey(name: 'asset_id')
  String? get assetId => throw _privateConstructorUsedError;
  @override
  String? get url => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$CryptoAssetModelCopyWith<_CryptoAssetModel> get copyWith =>
      throw _privateConstructorUsedError;
}
