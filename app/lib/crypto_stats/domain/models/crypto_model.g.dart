// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crypto_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CryptoModel _$$_CryptoModelFromJson(Map<String, dynamic> json) =>
    _$_CryptoModel(
      timeExchange: json['time_exchange'] as String?,
      timeCoinapi: json['time_coinapi'] as String?,
      uuid: json['uuid'] as String?,
      price: (json['price'] as num?)?.toDouble(),
      size: (json['size'] as num?)?.toDouble(),
      takerSide: json['taker_side'] as String?,
      symbolId: json['symbol_id'] as String?,
      sequence: json['sequence'] as int?,
      type: json['type'] as String?,
    );

Map<String, dynamic> _$$_CryptoModelToJson(_$_CryptoModel instance) =>
    <String, dynamic>{
      'time_exchange': instance.timeExchange,
      'time_coinapi': instance.timeCoinapi,
      'uuid': instance.uuid,
      'price': instance.price,
      'size': instance.size,
      'taker_side': instance.takerSide,
      'symbol_id': instance.symbolId,
      'sequence': instance.sequence,
      'type': instance.type,
    };
