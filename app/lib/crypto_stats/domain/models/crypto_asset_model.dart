import 'package:freezed_annotation/freezed_annotation.dart';

part 'crypto_asset_model.freezed.dart';
part 'crypto_asset_model.g.dart';

@freezed
class CryptoAssetModel with _$CryptoAssetModel {
  const factory CryptoAssetModel(
      {@JsonKey(name: 'asset_id') String? assetId,
      String? url}) = _CryptoAssetModel;

  factory CryptoAssetModel.fromJson(Map<String, dynamic> json) =>
      _$CryptoAssetModelFromJson(json);
}
