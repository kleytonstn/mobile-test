import 'package:freezed_annotation/freezed_annotation.dart';

part 'crypto_model.freezed.dart';
part 'crypto_model.g.dart';

@freezed
class CryptoModel with _$CryptoModel {
  const factory CryptoModel({
    @JsonKey(name: 'time_exchange') String? timeExchange,
    @JsonKey(name: 'time_coinapi') String? timeCoinapi,
    String? uuid,
    double? price,
    double? size,
    @JsonKey(name: 'taker_side') String? takerSide,
    @JsonKey(name: 'symbol_id') String? symbolId,
    int? sequence,
    String? type,
  }) = _CryptoModel;

  factory CryptoModel.fromJson(Map<String, dynamic> json) =>
      _$CryptoModelFromJson(json);
}
