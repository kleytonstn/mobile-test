// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'message_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MessageModel _$MessageModelFromJson(Map<String, dynamic> json) {
  return _MessageModel.fromJson(json);
}

/// @nodoc
class _$MessageModelTearOff {
  const _$MessageModelTearOff();

  _MessageModel call(
      {String? type,
      String? apikey,
      bool? heartbeat,
      List<String>? subscribe_data_type,
      List<String>? subscribe_filter_symbol_id}) {
    return _MessageModel(
      type: type,
      apikey: apikey,
      heartbeat: heartbeat,
      subscribe_data_type: subscribe_data_type,
      subscribe_filter_symbol_id: subscribe_filter_symbol_id,
    );
  }

  MessageModel fromJson(Map<String, Object> json) {
    return MessageModel.fromJson(json);
  }
}

/// @nodoc
const $MessageModel = _$MessageModelTearOff();

/// @nodoc
mixin _$MessageModel {
  String? get type => throw _privateConstructorUsedError;
  String? get apikey => throw _privateConstructorUsedError;
  bool? get heartbeat => throw _privateConstructorUsedError;
  List<String>? get subscribe_data_type => throw _privateConstructorUsedError;
  List<String>? get subscribe_filter_symbol_id =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MessageModelCopyWith<MessageModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageModelCopyWith<$Res> {
  factory $MessageModelCopyWith(
          MessageModel value, $Res Function(MessageModel) then) =
      _$MessageModelCopyWithImpl<$Res>;
  $Res call(
      {String? type,
      String? apikey,
      bool? heartbeat,
      List<String>? subscribe_data_type,
      List<String>? subscribe_filter_symbol_id});
}

/// @nodoc
class _$MessageModelCopyWithImpl<$Res> implements $MessageModelCopyWith<$Res> {
  _$MessageModelCopyWithImpl(this._value, this._then);

  final MessageModel _value;
  // ignore: unused_field
  final $Res Function(MessageModel) _then;

  @override
  $Res call({
    Object? type = freezed,
    Object? apikey = freezed,
    Object? heartbeat = freezed,
    Object? subscribe_data_type = freezed,
    Object? subscribe_filter_symbol_id = freezed,
  }) {
    return _then(_value.copyWith(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      apikey: apikey == freezed
          ? _value.apikey
          : apikey // ignore: cast_nullable_to_non_nullable
              as String?,
      heartbeat: heartbeat == freezed
          ? _value.heartbeat
          : heartbeat // ignore: cast_nullable_to_non_nullable
              as bool?,
      subscribe_data_type: subscribe_data_type == freezed
          ? _value.subscribe_data_type
          : subscribe_data_type // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      subscribe_filter_symbol_id: subscribe_filter_symbol_id == freezed
          ? _value.subscribe_filter_symbol_id
          : subscribe_filter_symbol_id // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
abstract class _$MessageModelCopyWith<$Res>
    implements $MessageModelCopyWith<$Res> {
  factory _$MessageModelCopyWith(
          _MessageModel value, $Res Function(_MessageModel) then) =
      __$MessageModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? type,
      String? apikey,
      bool? heartbeat,
      List<String>? subscribe_data_type,
      List<String>? subscribe_filter_symbol_id});
}

/// @nodoc
class __$MessageModelCopyWithImpl<$Res> extends _$MessageModelCopyWithImpl<$Res>
    implements _$MessageModelCopyWith<$Res> {
  __$MessageModelCopyWithImpl(
      _MessageModel _value, $Res Function(_MessageModel) _then)
      : super(_value, (v) => _then(v as _MessageModel));

  @override
  _MessageModel get _value => super._value as _MessageModel;

  @override
  $Res call({
    Object? type = freezed,
    Object? apikey = freezed,
    Object? heartbeat = freezed,
    Object? subscribe_data_type = freezed,
    Object? subscribe_filter_symbol_id = freezed,
  }) {
    return _then(_MessageModel(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      apikey: apikey == freezed
          ? _value.apikey
          : apikey // ignore: cast_nullable_to_non_nullable
              as String?,
      heartbeat: heartbeat == freezed
          ? _value.heartbeat
          : heartbeat // ignore: cast_nullable_to_non_nullable
              as bool?,
      subscribe_data_type: subscribe_data_type == freezed
          ? _value.subscribe_data_type
          : subscribe_data_type // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      subscribe_filter_symbol_id: subscribe_filter_symbol_id == freezed
          ? _value.subscribe_filter_symbol_id
          : subscribe_filter_symbol_id // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MessageModel implements _MessageModel {
  const _$_MessageModel(
      {this.type,
      this.apikey,
      this.heartbeat,
      this.subscribe_data_type,
      this.subscribe_filter_symbol_id});

  factory _$_MessageModel.fromJson(Map<String, dynamic> json) =>
      _$$_MessageModelFromJson(json);

  @override
  final String? type;
  @override
  final String? apikey;
  @override
  final bool? heartbeat;
  @override
  final List<String>? subscribe_data_type;
  @override
  final List<String>? subscribe_filter_symbol_id;

  @override
  String toString() {
    return 'MessageModel(type: $type, apikey: $apikey, heartbeat: $heartbeat, subscribe_data_type: $subscribe_data_type, subscribe_filter_symbol_id: $subscribe_filter_symbol_id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MessageModel &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.apikey, apikey) ||
                const DeepCollectionEquality().equals(other.apikey, apikey)) &&
            (identical(other.heartbeat, heartbeat) ||
                const DeepCollectionEquality()
                    .equals(other.heartbeat, heartbeat)) &&
            (identical(other.subscribe_data_type, subscribe_data_type) ||
                const DeepCollectionEquality()
                    .equals(other.subscribe_data_type, subscribe_data_type)) &&
            (identical(other.subscribe_filter_symbol_id,
                    subscribe_filter_symbol_id) ||
                const DeepCollectionEquality().equals(
                    other.subscribe_filter_symbol_id,
                    subscribe_filter_symbol_id)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(apikey) ^
      const DeepCollectionEquality().hash(heartbeat) ^
      const DeepCollectionEquality().hash(subscribe_data_type) ^
      const DeepCollectionEquality().hash(subscribe_filter_symbol_id);

  @JsonKey(ignore: true)
  @override
  _$MessageModelCopyWith<_MessageModel> get copyWith =>
      __$MessageModelCopyWithImpl<_MessageModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MessageModelToJson(this);
  }
}

abstract class _MessageModel implements MessageModel {
  const factory _MessageModel(
      {String? type,
      String? apikey,
      bool? heartbeat,
      List<String>? subscribe_data_type,
      List<String>? subscribe_filter_symbol_id}) = _$_MessageModel;

  factory _MessageModel.fromJson(Map<String, dynamic> json) =
      _$_MessageModel.fromJson;

  @override
  String? get type => throw _privateConstructorUsedError;
  @override
  String? get apikey => throw _privateConstructorUsedError;
  @override
  bool? get heartbeat => throw _privateConstructorUsedError;
  @override
  List<String>? get subscribe_data_type => throw _privateConstructorUsedError;
  @override
  List<String>? get subscribe_filter_symbol_id =>
      throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MessageModelCopyWith<_MessageModel> get copyWith =>
      throw _privateConstructorUsedError;
}
