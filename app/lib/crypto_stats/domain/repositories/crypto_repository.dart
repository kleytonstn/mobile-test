import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:crypto_stats/crypto_stats/domain/models/message_model.dart';

abstract class CryptoRepository {
  Future<Stream> getCryptoStream(String url, MessageModel message);
  Future<List<CryptoAssetModel>> getCryptoAssets();
}
