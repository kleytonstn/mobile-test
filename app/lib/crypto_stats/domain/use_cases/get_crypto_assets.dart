import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:crypto_stats/crypto_stats/domain/repositories/crypto_repository.dart';

class GetCryptoAssetsUseCase {
  GetCryptoAssetsUseCase(this._cryptoRepository);
  final CryptoRepository _cryptoRepository;

  Future<List<CryptoAssetModel>> call() {
    return _cryptoRepository.getCryptoAssets();
  }
}
