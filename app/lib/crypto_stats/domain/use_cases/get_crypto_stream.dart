import 'package:crypto_stats/crypto_stats/data/constants/endpoints.dart';
import 'package:crypto_stats/crypto_stats/data/constants/ws_messages.dart';
import 'package:crypto_stats/crypto_stats/domain/repositories/crypto_repository.dart';

class GetCryptoStreamUseCase {
  GetCryptoStreamUseCase(this._cryptoRepository);
  final CryptoRepository _cryptoRepository;

  Future<Stream> call() {
    return _cryptoRepository.getCryptoStream(
        Endpoints.cryptoWebSocket, WsMessages.cryptoMessage);
  }
}
