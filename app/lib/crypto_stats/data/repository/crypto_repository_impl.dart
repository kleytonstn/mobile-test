import 'package:crypto_stats/crypto_stats/data/datasources/crypto_remote_datasource.dart';
import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:crypto_stats/crypto_stats/domain/models/message_model.dart';
import 'package:crypto_stats/crypto_stats/domain/repositories/crypto_repository.dart';

class CryptoRepositoryImpl implements CryptoRepository {
  CryptoRepositoryImpl(this._cryptoRemoteDataSource);
  final CryptoRemoteDataSource _cryptoRemoteDataSource;

  @override
  Future<Stream> getCryptoStream(String url, MessageModel message) {
    return _cryptoRemoteDataSource.getCryptoStream(url, message);
  }

  @override
  Future<List<CryptoAssetModel>> getCryptoAssets() async {
    final response = await _cryptoRemoteDataSource.getCryptoAssets();
    return response
        .map((dynamic asset) =>
            CryptoAssetModel.fromJson(asset as Map<String, dynamic>))
        .toList();
  }
}
