class StringConstants {
  static const String envKey = 'key';
  static const String xCoinApiKey = 'X-CoinAPI-Key';
  static const String btcId = 'COINBASE_SPOT_BTC_USD';
  static const String ethId = 'COINBASE_SPOT_ETH_USD';
  static const String adaId = 'COINBASE_SPOT_ADA_USD';
  static const String usdtId = 'COINBASE_SPOT_USDT_USD';
  static const String dogeId = 'COINBASE_SPOT_DOGE_USD';
  static const String btc = 'BTC';
  static const String eth = 'ETH';
  static const String ada = 'ADA';
  static const String usdt = 'USDT';
  static const String doge = 'DOGE';
  static const String usd = '/USD';
}
