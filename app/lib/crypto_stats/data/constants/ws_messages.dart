import 'package:crypto_stats/crypto_stats/domain/models/message_model.dart';

class WsMessages {
  static const cryptoMessage = MessageModel(
    type: "hello",
    heartbeat: false,
    subscribe_data_type: ["trade"],
    subscribe_filter_symbol_id: [
      r'COINBASE_SPOT_BTC_USD$',
      r'COINBASE_SPOT_ETH_USD$',
      r'COINBASE_SPOT_ADA_USD$',
      r'COINBASE_SPOT_USDT_USD$',
      r'COINBASE_SPOT_DOGE_USD$',
    ],
  );
}
