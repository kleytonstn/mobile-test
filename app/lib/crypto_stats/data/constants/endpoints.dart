class Endpoints {
  static const String cryptoWebSocket = 'wss://ws-sandbox.coinapi.io/v1/';
  static const String assetsEndpoint =
      'https://rest.coinapi.io/v1/assets/icons/100';
}
