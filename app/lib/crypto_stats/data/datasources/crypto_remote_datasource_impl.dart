import 'dart:convert';
import 'dart:io';

import 'package:crypto_stats/crypto_stats/data/constants/endpoints.dart';
import 'package:crypto_stats/crypto_stats/data/constants/strings_constants.dart';
import 'package:crypto_stats/crypto_stats/data/datasources/crypto_remote_datasource.dart';
import 'package:crypto_stats/crypto_stats/data/utils/utils.dart';
import 'package:crypto_stats/crypto_stats/domain/models/message_model.dart';
import 'package:dio/dio.dart';

class CryptoRemoteDataSourceImpl implements CryptoRemoteDataSource {
  CryptoRemoteDataSourceImpl(this._apiKey);
  final String _apiKey;

  @override
  Future<Stream> getCryptoStream(String url, MessageModel message) async {
    final socket = await WebSocket.connect(Uri.parse(url).toString());
    socket.add(json.encode(message.withKey(_apiKey).toJson()));
    return socket;
  }

  @override
  Future<List<dynamic>> getCryptoAssets() async {
    final response = await Dio().get<List<dynamic>>(Endpoints.assetsEndpoint,
        options: Options(
            headers: <String, String>{StringConstants.xCoinApiKey: _apiKey}));
    return response.data!;
  }
}
