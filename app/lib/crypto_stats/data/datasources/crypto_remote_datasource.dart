import 'package:crypto_stats/crypto_stats/domain/models/message_model.dart';

abstract class CryptoRemoteDataSource {
  Future<Stream> getCryptoStream(String url, MessageModel message);
  Future<List<dynamic>> getCryptoAssets();
}
