import 'package:crypto_stats/crypto_stats/data/constants/strings_constants.dart';
import 'package:crypto_stats/crypto_stats/domain/models/crypto_model.dart';
import 'package:crypto_stats/crypto_stats/domain/models/message_model.dart';

extension AssignKey on MessageModel {
  MessageModel withKey(String key) => copyWith(apikey: key);
}

extension Supported on CryptoModel {
  bool isSupported() =>
      symbolId == StringConstants.btcId ||
      symbolId == StringConstants.ethId ||
      symbolId == StringConstants.adaId ||
      symbolId == StringConstants.usdtId ||
      symbolId == StringConstants.dogeId;
}
