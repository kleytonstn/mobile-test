import 'package:flutter/material.dart';

class CryptoColors {
  static const Color background = Color(0xFFFAFAFA);
  static const Color balanceBackground = Color(0xFF516DFB);
  static const Color positiveColor = Color(0xFF64EDB0);
  static const Color negativeColor = Color(0xFFFA2F48);
}
