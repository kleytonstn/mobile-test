import 'package:crypto_stats/crypto_stats/data/constants/strings_constants.dart';
import 'package:crypto_stats/crypto_stats/domain/models/crypto_model.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_cubit.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/live_price.dart';
import 'package:crypto_stats/generated/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LivePrices extends StatefulWidget {
  const LivePrices({Key? key}) : super(key: key);

  @override
  State<LivePrices> createState() => _LivePricesState();
}

class _LivePricesState extends State<LivePrices> {
  late double? btcLast = 0;
  late double? ethLast = 0;
  late double? adaLast = 0;
  late double? usdtLast = 0;
  late double? dogeLast = 0;

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<CryptoCubit>(context);
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          StreamBuilder<CryptoModel>(
            stream: cubit.btcStream.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              final livePrice = LivePrice(
                  Assets.images.btc.image(width: 50, height: 50),
                  StringConstants.btc,
                  snapshot.data!.price!,
                  snapshot.data!.takerSide!,
                  btcLast! < snapshot.data!.price!);
              btcLast = snapshot.data!.price;
              return livePrice;
            },
          ),
          StreamBuilder<CryptoModel>(
            stream: cubit.ethStream.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              final livePrice = LivePrice(
                  Assets.images.eth.image(width: 50, height: 50),
                  StringConstants.eth,
                  snapshot.data!.price!,
                  snapshot.data!.takerSide!,
                  ethLast! < snapshot.data!.price!);
              ethLast = snapshot.data!.price;
              return livePrice;
            },
          ),
          StreamBuilder<CryptoModel>(
            stream: cubit.adaStream.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              final livePrice = LivePrice(
                  Assets.images.ada.image(width: 50, height: 50),
                  StringConstants.ada,
                  snapshot.data!.price!,
                  snapshot.data!.takerSide!,
                  adaLast! < snapshot.data!.price!);
              adaLast = snapshot.data!.price;
              return livePrice;
            },
          ),
          StreamBuilder<CryptoModel>(
            stream: cubit.usdtStream.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              final livePrice = LivePrice(
                  Assets.images.usdt.image(width: 50, height: 50),
                  StringConstants.usdt,
                  snapshot.data!.price!,
                  snapshot.data!.takerSide!,
                  usdtLast! < snapshot.data!.price!);
              usdtLast = snapshot.data!.price;
              return livePrice;
            },
          ),
          StreamBuilder<CryptoModel>(
            stream: cubit.dogeStream.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              final livePrice = LivePrice(
                  Assets.images.doge.image(width: 50, height: 50),
                  StringConstants.doge,
                  snapshot.data!.price!,
                  snapshot.data!.takerSide!,
                  dogeLast! < snapshot.data!.price!);
              dogeLast = snapshot.data!.price;
              return livePrice;
            },
          ),
        ],
      ),
    );
  }
}
