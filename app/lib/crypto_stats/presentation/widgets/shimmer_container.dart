import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerContainer extends StatelessWidget {
  const ShimmerContainer({Key? key, this.height, this.width, this.radius})
      : super(key: key);

  final double? height;
  final double? width;
  final double? radius;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: const Color(0xFFDCE1E5),
      highlightColor: const Color(0xFFF3F6F9),
      child: Opacity(
        opacity: 0.5,
        child: Container(
          width: width ?? double.infinity,
          height: height ?? 50,
          decoration: BoxDecoration(
            color: Colors.grey.shade700,
            borderRadius: BorderRadius.circular(radius ?? 8),
          ),
        ),
      ),
    );
  }
}
