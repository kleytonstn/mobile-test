import 'package:crypto_stats/generated/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppHeader extends StatelessWidget {
  const AppHeader({Key? key, required this.message, required this.username})
      : super(key: key);
  final String message;
  final String username;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(message, style: Theme.of(context).textTheme.bodyText1),
                Text(username,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(fontWeight: FontWeight.bold, fontSize: 20)),
              ],
            ),
            CircleAvatar(
              radius: 24,
              backgroundImage: Assets.images.avatar.image().image,
            ),
          ],
        ),
      ],
    );
  }
}
