import 'package:crypto_stats/crypto_stats/data/constants/strings_constants.dart';
import 'package:crypto_stats/crypto_stats/presentation/colors/crypto_colors.dart';
import 'package:flutter/material.dart';

class LivePrice extends StatefulWidget {
  LivePrice(this.assetImage, this.assetName, this.assetPrice,
      this.operationType, this.greaterPrice);
  final Image assetImage;
  final String assetName;
  final double assetPrice;
  final String operationType;
  final bool greaterPrice;

  @override
  State<LivePrice> createState() => _LivePriceState();
}

class _LivePriceState extends State<LivePrice> with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Color?> _animation;
  bool animationUpdate = false;

  @override
  Widget build(BuildContext context) {
    if (!animationUpdate) {
      _controller = AnimationController(
          vsync: this, duration: const Duration(milliseconds: 1000))
        ..forward();
      _animation = ColorTween(
              begin: widget.greaterPrice
                  ? CryptoColors.positiveColor
                  : CryptoColors.negativeColor,
              end: Colors.black)
          .animate(_controller)
        ..addListener(() {
          setState(() {
            animationUpdate = true;
          });
        });
    }

    animationUpdate = false;

    return Card(
      child: SizedBox(
        width: 250,
        height: 180,
        child: Stack(
          children: [
            Positioned(
              top: 15,
              left: 15,
              child: Row(
                children: [
                  widget.assetImage,
                  const SizedBox(width: 14),
                  RichText(
                    text: TextSpan(
                        text: widget.assetName,
                        style: Theme.of(context).textTheme.bodyText1?.copyWith(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                        children: [
                          TextSpan(
                              text: StringConstants.usd,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(
                                    fontSize: 18,
                                  ))
                        ]),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.assetPrice.toString(),
                    style: Theme.of(context).textTheme.headline5?.copyWith(
                          fontWeight: FontWeight.bold,
                          color: _animation.value,
                        ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 15,
              left: 15,
              child: Text(
                widget.operationType,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
