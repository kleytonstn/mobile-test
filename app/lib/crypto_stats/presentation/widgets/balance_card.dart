import 'package:crypto_stats/crypto_stats/presentation/colors/crypto_colors.dart';
import 'package:crypto_stats/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BalanceCard extends StatelessWidget {
  const BalanceCard({this.keyValue, this.balance, this.profit, this.variation});
  final String? keyValue;
  final double? balance;
  final double? profit;
  final double? variation;

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    final format =
        NumberFormat.currency(locale: 'en', symbol: r'$', decimalDigits: 0);
    final formatPercent = NumberFormat.percentPattern('en');
    return Container(
      key: keyValue != null ? Key(keyValue!) : null,
      height: 160,
      decoration: BoxDecoration(
        color: CryptoColors.balanceBackground,
        borderRadius: BorderRadius.circular(18),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(l10n.balance, style: Theme.of(context).textTheme.bodyText2),
            Text(
              format.format(balance),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  ?.copyWith(fontWeight: FontWeight.bold, color: Colors.white),
            ),
            const SizedBox(height: 14),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      l10n.monthlyProfit,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    Text(
                      format.format(profit),
                      style: Theme.of(context).textTheme.headline6?.copyWith(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ],
                ),
                Container(
                  width: 60,
                  height: 30,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(.2),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _variationItem(variation!),
                        Text(
                          formatPercent.format(variation),
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _variationItem(double variation) {
    return Padding(
      padding: const EdgeInsets.only(right: 4),
      child: Icon(
        variation.isNegative
            ? Icons.arrow_downward
            : Icons.arrow_upward_rounded,
        size: 10,
        color: variation.isNegative
            ? CryptoColors.negativeColor
            : CryptoColors.positiveColor,
      ),
    );
  }
}
