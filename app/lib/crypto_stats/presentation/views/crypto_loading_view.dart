import 'package:crypto_stats/crypto_stats/presentation/views/crypto_assets_loading_view.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/app_header.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/shimmer_container.dart';
import 'package:crypto_stats/l10n/l10n.dart';
import 'package:flutter/material.dart';

class CryptoLoadingView extends StatelessWidget {
  const CryptoLoadingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 40, bottom: 20),
            child: AppHeader(message: l10n.welcome, username: l10n.username),
          ),
          const ShimmerContainer(radius: 15, height: 170),
          const SizedBox(height: 40),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: const [
                ShimmerContainer(radius: 8, width: 230, height: 180),
                SizedBox(width: 10),
                ShimmerContainer(radius: 8, width: 230, height: 180),
              ],
            ),
          ),
          const SizedBox(height: 50),
          const CryptoAssetsLoadingView(),
        ],
      ),
    ));
  }
}
