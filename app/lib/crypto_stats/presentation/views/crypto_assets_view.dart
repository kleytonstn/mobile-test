import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:crypto_stats/crypto_stats/presentation/colors/crypto_colors.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_assets_cubit.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_assets_state.dart';
import 'package:crypto_stats/crypto_stats/presentation/views/crypto_assets_loading_view.dart';
import 'package:crypto_stats/crypto_stats/presentation/views/crypto_failure_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CryptoAssetsView extends StatefulWidget {
  const CryptoAssetsView({Key? key}) : super(key: key);

  @override
  State<CryptoAssetsView> createState() => _CryptoAssetsViewState();
}

class _CryptoAssetsViewState extends State<CryptoAssetsView> {
  late CryptoAssetsCubit _cryptoAssetsCubit;

  @override
  void initState() {
    _cryptoAssetsCubit = context.read<CryptoAssetsCubit>()..getCryptoAssets();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CryptoColors.background,
      body: SingleChildScrollView(
        child: BlocBuilder<CryptoAssetsCubit, CryptoAssetsState>(
          bloc: _cryptoAssetsCubit,
          builder: (context, state) => state.when(
            initial: () => const CryptoAssetsLoadingView(),
            loading: () => const CryptoAssetsLoadingView(),
            success: _buildList,
            failure: () => const CryptoFailureView(),
          ),
        ),
      ),
    );
  }

  Widget _buildList(List<CryptoAssetModel> assets) {
    return Column(
      children: assets
          .take(20)
          .map((asset) => Padding(
                padding: const EdgeInsets.only(bottom: 2),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: SizedBox(
                      width: double.infinity,
                      height: 40,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10,
                            child: Image.network(asset.url!,
                                height: 40, width: 40),
                          ),
                          Center(
                            child: Text(
                              asset.assetId!,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ))
          .toList(),
    );
  }
}
