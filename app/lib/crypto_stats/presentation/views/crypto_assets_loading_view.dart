import 'package:crypto_stats/crypto_stats/presentation/widgets/shimmer_container.dart';
import 'package:flutter/material.dart';

class CryptoAssetsLoadingView extends StatelessWidget {
  const CryptoAssetsLoadingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          4,
          (index) => const Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: ShimmerContainer(radius: 8, height: 50),
              )),
    );
  }
}
