import 'package:crypto_stats/crypto_stats/presentation/views/crypto_assets_view.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/app_header.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/balance_card.dart';
import 'package:crypto_stats/crypto_stats/presentation/widgets/live_prices.dart';
import 'package:crypto_stats/l10n/l10n.dart';
import 'package:flutter/material.dart';

class CryptoStatsView extends StatefulWidget {
  const CryptoStatsView({Key? key}) : super(key: key);

  @override
  _CryptoStatsViewState createState() => _CryptoStatsViewState();
}

class _CryptoStatsViewState extends State<CryptoStatsView> {
  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40, bottom: 20),
                child:
                    AppHeader(message: l10n.welcome, username: l10n.username),
              ),
              const BalanceCard(
                balance: 450000,
                profit: 10000,
                variation: 0.1,
              ),
              Container(height: 20),
              Text(
                context.l10n.livePrices,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              const SizedBox(height: 10),
              const LivePrices(),
              const SizedBox(height: 20),
              Text(
                context.l10n.cryptoAssets,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              const SizedBox(height: 10),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.4,
                child: const CryptoAssetsView(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
