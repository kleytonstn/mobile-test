import 'package:crypto_stats/crypto_stats/presentation/colors/crypto_colors.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_cubit.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_state.dart';
import 'package:crypto_stats/crypto_stats/presentation/views/crypto_failure_view.dart';
import 'package:crypto_stats/crypto_stats/presentation/views/crypto_loading_view.dart';
import 'package:crypto_stats/crypto_stats/presentation/views/crypto_stats_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// {@template crypto_stats_page}
///  Page that hanldes the user interface for crypto stats feature.
/// {@endtemplate}
class CryptoStatsPage extends StatefulWidget {
  /// {@macro crypto_stats_page}
  const CryptoStatsPage({Key? key}) : super(key: key);

  @override
  State<CryptoStatsPage> createState() => _CryptoStatsPageState();
}

class _CryptoStatsPageState extends State<CryptoStatsPage> {
  late CryptoCubit _cryptoCubit;

  @override
  void initState() {
    _cryptoCubit = context.read<CryptoCubit>()..getCryptoStream();
    super.initState();
  }

  @override
  void dispose() {
    _cryptoCubit.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CryptoColors.background,
      body: BlocBuilder<CryptoCubit, CryptoState>(
        bloc: _cryptoCubit,
        builder: (context, state) => state.when(
          initial: () => const CryptoLoadingView(),
          loading: () => const CryptoLoadingView(),
          success: () => const CryptoStatsView(),
          failure: () => const CryptoFailureView(),
        ),
      ),
    );
  }
}
