import 'package:crypto_stats/l10n/l10n.dart';
import 'package:flutter/material.dart';

class CryptoFailureView extends StatelessWidget {
  const CryptoFailureView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Text(
            context.l10n.errorMessage,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ],
    );
  }
}
