import 'package:freezed_annotation/freezed_annotation.dart';

part 'crypto_state.freezed.dart';

@freezed
class CryptoState with _$CryptoState {
  const factory CryptoState.initial() = _Initial;
  const factory CryptoState.loading() = _Loading;
  const factory CryptoState.success() = _Success;
  const factory CryptoState.failure() = _Failure;
}
