import 'package:crypto_stats/crypto_stats/domain/models/crypto_asset_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'crypto_assets_state.freezed.dart';

@freezed
class CryptoAssetsState with _$CryptoAssetsState {
  const factory CryptoAssetsState.initial() = _Initial;
  const factory CryptoAssetsState.loading() = _Loading;
  const factory CryptoAssetsState.success(List<CryptoAssetModel> assets) =
      _Success;
  const factory CryptoAssetsState.failure() = _Failure;
}
