import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_assets.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_assets_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CryptoAssetsCubit extends Cubit<CryptoAssetsState> {
  CryptoAssetsCubit(this._getCryptoAssetsUseCase)
      : super(const CryptoAssetsState.initial());

  final GetCryptoAssetsUseCase _getCryptoAssetsUseCase;

  Future<void> getCryptoAssets() async {
    emit(const CryptoAssetsState.loading());
    try {
      final assets = await _getCryptoAssetsUseCase.call();
      emit(CryptoAssetsState.success(assets));
    } catch (_) {
      emit(const CryptoAssetsState.failure());
      rethrow;
    }
  }
}
