import 'dart:async';
import 'dart:convert';

import 'package:crypto_stats/crypto_stats/data/constants/strings_constants.dart';
import 'package:crypto_stats/crypto_stats/data/utils/utils.dart';
import 'package:crypto_stats/crypto_stats/domain/models/crypto_model.dart';
import 'package:crypto_stats/crypto_stats/domain/use_cases/get_crypto_stream.dart';
import 'package:crypto_stats/crypto_stats/presentation/cubits/crypto_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CryptoCubit extends Cubit<CryptoState> {
  CryptoCubit(this._getCryptoStreamUseCase)
      : super(const CryptoState.initial());

  final GetCryptoStreamUseCase _getCryptoStreamUseCase;
  StreamController<CryptoModel> cryptos = StreamController<CryptoModel>();
  StreamController<CryptoModel> btcStream = StreamController.broadcast();
  StreamController<CryptoModel> ethStream = StreamController.broadcast();
  StreamController<CryptoModel> adaStream = StreamController.broadcast();
  StreamController<CryptoModel> usdtStream = StreamController.broadcast();
  StreamController<CryptoModel> dogeStream = StreamController.broadcast();

  Future<void> getCryptoStream() async {
    Stream _cryptoStream;
    emit(const CryptoState.loading());
    try {
      _cryptoStream = await _getCryptoStreamUseCase();
      _listenMainStream(_cryptoStream);
      _listenCryptoStreams();
      emit(const CryptoState.success());
    } catch (_) {
      emit(const CryptoState.failure());
      rethrow;
    }
  }

  void _listenMainStream(Stream stream) {
    stream.listen((dynamic data) {
      final crypto = CryptoModel.fromJson(
          jsonDecode(data as String) as Map<String, dynamic>);
      if (crypto.isSupported()) {
        cryptos.add(crypto);
      }
    });
  }

  void _listenCryptoStreams() {
    cryptos.stream.listen((CryptoModel crypto) {
      if (crypto.isSupported()) {
        switch (crypto.symbolId) {
          case StringConstants.btcId:
            btcStream.add(crypto);
            break;
          case StringConstants.ethId:
            ethStream.add(crypto);
            break;
          case StringConstants.adaId:
            adaStream.add(crypto);
            break;
          case StringConstants.usdtId:
            usdtStream.add(crypto);
            break;
          case StringConstants.dogeId:
            dogeStream.add(crypto);
            break;
        }
      }
    });
  }

  void dispose() {
    cryptos.close();
    btcStream.close();
    ethStream.close();
    adaStream.close();
  }
}
